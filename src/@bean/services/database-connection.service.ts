import { Injectable } from "@nestjs/common";
import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm';

/**
 * Database connection service config
 */
@Injectable()
export class DatabaseConnectionService implements TypeOrmOptionsFactory {
  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      name: 'default',
      type: 'mysql',
      host: process.env.AWS_DATABASE_HOST,
      port: Number(process.env.DATABASE_PORT),
      username: process.env.AWS_DATABASE_USERNAME,
      password: process.env.AWS_DATABASE_PASSWORD,
      database: process.env.AWS_DATABASE_DBNAME,      
      synchronize: false,      
      dropSchema: false,
      logging: true,
      entities: ['dist/**/*.entity.js'],
    };
  }
}